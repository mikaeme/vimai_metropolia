# VimAI Navigation Metropolia client

This is a VimAI Android custom client for Metropolia Myyrmäki Campus. The client is built on the the [VmAI example client](https://gitlab.com/vimai-public/vimai-android-client-example/-/tree/master/).

Content for POIs (Points of Interest) can be created and edited with CustomAdapters.

3D-models:
```
   override fun createRenderable(context: Context, poi: Poi): CompletableFuture<out Renderable?> {  
        return ModelRenderable.Builder().setSource(context, Uri.parse("YOUR-3D-FILE.sfb")).build()
    }
```
YOUR-3D-FILE.sfb should be located in assets folder

Text content:

    override fun createRenderable(context: Context, poi: Poi): CompletableFuture<out Renderable?> {
            val textView = TextView(context)
            textView.text = TEXT CONTENT HERE
            return ViewRenderable.builder().setView(context, textView).build()
    }


CustomAdapters must be registered in the NavigationFragment:

```       
registerPoiTypeAdapter("NAME OF POI", CustomAdapterYOUR-IDENTIFIER::class.java)
```
The name of the POI must exactly match the name on the map. Remember to import you adapter.


To display dynamic content, API calls can be placed in the MainActivity:

            doAsync {
                    Singleton.example = URL("http://your-api-request").readText()
                    uiThread {
                }
            }

Remember to declare variable "example" in the Singleton object on top to make it visible in CustomAdapter (`textView.text = MainActivity.Singleton.example`).



Written entirely in Kotlin, uses [androidx](https://developer.android.com/jetpack) components.

It uses Sceneform engine for rendering, ARCore is necessary for the positioning to work. For the more in-detail instructions how to use the SDK, check the NavigationFragment.kt file. 
