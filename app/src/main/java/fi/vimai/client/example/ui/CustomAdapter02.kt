import android.content.Context
import android.widget.TextView
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.Renderable
import com.google.ar.sceneform.rendering.ViewRenderable
import fi.vimai.client.example.MainActivity
import fi.vimai.client.example.R
import fi.vimai.navigation.ar.adapters.ArPoiTypeAdapter
import fi.vimai.navigation.server.entities.Poi
import java.util.concurrent.CompletableFuture

class CustomAdapter02: ArPoiTypeAdapter {

    companion object {

        private const val DEFAULT_TEXT_POI_POSITION_Y = 1f
    }
    val initial = Quaternion()
    val rotationDelta = Quaternion(Vector3(0f, 1f, 0f), 30f)
        //the values above provided for example and not necessary

    override fun createRenderable(context: Context, poi: Poi): CompletableFuture<out Renderable?> {

            val textView = TextView(context)
            textView.setTextAppearance(R.style.HslTextAppearance)
            textView.setBackgroundResource(R.color.hsl_blue);
            textView.text = MainActivity.Singleton.hsl  //PUT TEXT CONTENT HERE
            return ViewRenderable.builder().setView(context, textView).build()
    }

    override fun update(node: Node, userPosition: Vector3, userRotation: Quaternion, frameTime: FrameTime) {
//describe the possible behavior of the model using node: position, rotation, etc. For example:
        node.localPosition = Vector3(node.localPosition.x, DEFAULT_TEXT_POI_POSITION_Y, node.localPosition.z)
        //val zz = node.worldPosition.z
        val localDirection = Vector3(userPosition.x - node.worldPosition.x, 0f, userPosition.z - node.worldPosition.z) //zz-180f
        node.setLookDirection(localDirection)
//or
        //   node.localRotation = Quaternion.multiply(node.localRotation,
        //           Quaternion.slerp(initial, rotationDelta, frameTime.deltaSeconds))
    }
}