package fi.vimai.client.example

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import fi.vimai.navigation.service.VimaiService
import fi.vimai.navigation.utils.ARUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.doAsyncResult
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import java.net.URL

class MainActivity : AppCompatActivity() {

    // Create singleton object that contains text data fetched from external API
    object Singleton{

        init {
            println("Singleton class invoked.")
        }
        var sodexo = "sodexo"
        var hsl = "hsl"
        var info = "info"
    }

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        VimaiService.configuration.debug = true   //show debug information
//        VimaiService.configuration.simulatePositionsViaMinimap = true //simulate positions by pressing on a minimap

        if (!ARUtils.checkIsSupportedDeviceOrFinish(this)) {
            return
        }
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val url = getString(R.string.vimai_base_url)
        Log.d("vimai", "Using $url server address")

        val navController = findNavController(R.id.nav_host_fragment)

        //Asynchronous API call utlizing Anko library

            doAsync {
                    Singleton.sodexo = URL("http://10.114.34.38/app/menu/").readText()
                    Singleton.hsl = URL("http://10.114.34.38/app/hsl/").readText()
                    Singleton.info = URL("http://10.114.34.38/app/info/").readText()
                    uiThread {
                        //              toast(result)
                }
            }

        appBarConfiguration = AppBarConfiguration(setOf(R.id.overviewFragment))
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


}
