package fi.vimai.client.example.ui

import android.view.View
import com.google.android.material.snackbar.Snackbar
import fi.vimai.client.example.R
import androidx.databinding.BindingAdapter



fun showError(parent: View, message: String) {
    Snackbar.make(parent, message, Snackbar.LENGTH_SHORT).show()
}

fun showGenericError(parent: View) {
    Snackbar.make(parent, parent.context.getString(R.string.error_generic), Snackbar.LENGTH_SHORT).show()
}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("android:invisibility")
fun setInvisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.INVISIBLE else View.VISIBLE
}