import android.content.Context
import android.net.Uri
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.Renderable
import fi.vimai.navigation.ar.adapters.ArPoiTypeAdapter
import fi.vimai.navigation.server.entities.Poi
import java.util.concurrent.CompletableFuture

class CustomAdapter04: ArPoiTypeAdapter {

    companion object {

        private const val DEFAULT_TEXT_POI_POSITION_Y = 1f

    }

    val initial = Quaternion()
    val rotationDelta = Quaternion(Vector3(0f, 1f, 0f), 30f)

    //the values above provided for example and not necessary
    override fun createRenderable(context: Context, poi: Poi): CompletableFuture<out Renderable?> {

        return ModelRenderable.Builder().setSource(context, Uri.parse("electria.sfb")).build()
    }

    override fun update(node: Node, userPosition: Vector3, userRotation: Quaternion, frameTime: FrameTime) {
//describe the possible behavior of the model using node: position, rotation, etc. For example:
        //node.localRotation = Quaternion.multiply(node.localRotation, Quaternion(Vector3(0f, 0f, 1f), 180f))
        node.localPosition = Vector3(node.localPosition.x, DEFAULT_TEXT_POI_POSITION_Y, node.localPosition.z)
        val localDirection = Vector3(-(userPosition.x - node.worldPosition.x), 0f, -(userPosition.z - node.worldPosition.z))
        node.setLookDirection(localDirection)
//or
// node.localRotation = Quaternion.multiply(node.localRotation,
        //        Quaternion.slerp(initial, rotationDelta, frameTime.deltaSeconds))
    }
}