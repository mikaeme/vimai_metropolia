package fi.vimai.client.example.ui

import CustomAdapter01
import CustomAdapter02
import CustomAdapter03
import CustomAdapter04
import CustomAdapter05
import CustomAdapter06
import CustomAdapter07
import CustomAdapter08
import CustomAdapter09
import CustomAdapter10
import CustomAdapter11
import CustomAdapter12
import CustomAdapter13
import CustomAdapter14
import CustomAdapter15

import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.rendering.Color
import com.google.ar.sceneform.rendering.MaterialFactory
import com.google.ar.sceneform.rendering.ModelRenderable

import fi.vimai.client.example.R
import fi.vimai.client.example.databinding.FragmentNavigationBinding
import fi.vimai.navigation.ar.NavigationArFragment
import fi.vimai.navigation.minimap.MinimapFragment
import fi.vimai.navigation.minimap.views.DestinationView
import fi.vimai.navigation.minimap.views.UserLocationView
import fi.vimai.navigation.routing.events.NavigationEnded
import fi.vimai.navigation.routing.events.NavigationReRouted
import fi.vimai.navigation.routing.events.NavigationStarted
import fi.vimai.navigation.routing.navigation.NavigationProgress
import fi.vimai.navigation.server.entities.Poi
import fi.vimai.navigation.service.VimaiService
import fi.vimai.navigation.utils.getBitmapFromVectorDrawable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.CompletableFuture
import androidx.cursoradapter.widget.CursorAdapter
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.navigation.fragment.findNavController
import fi.vimai.navigation.ar.NavigationArrowNode
import fi.vimai.navigation.routing.exceptions.CannotReachDestinationException
import fi.vimai.navigation.routing.exceptions.DestinationTooCloseException
import fi.vimai.navigation.routing.exceptions.UserPositionNotAvailableException
import fi.vimai.navigation.ui.PoiSearchWrapper
import fi.vimai.navigation.utils.showUserError


/**
 * A simple [Fragment] subclass.
 */
class NavigationFragment : NavigationArFragment() {

    private companion object {
        private const val MINIMAP_FRAGMENT_TAG = "MINIMAP_FRAGMENT_TAG"
    }

    private var destinationPoi: Poi? = null

    //something to render
    private var pinRenderable: ModelRenderable? = null
    private var resources3D: CompletableFuture<Void>? = null

    private lateinit var binding: FragmentNavigationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        showKeepPhoneUpHint = true

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_navigation, container, false)
        //make sure we add the AR content to our layout
        binding.arFragmentHolder.addView(super.onCreateView(inflater, container, savedInstanceState))

        binding.ivShowMinimap.setOnClickListener {
            showMinimap(true)
        }
        binding.ivHideMinimap.setOnClickListener {
            showMinimap(false)
        }
        //TODO added at onCreate in NavigationFragment, trying to change 3Dtext to any custom model:

            //Register CustomAdapters for POI content here

            registerPoiTypeAdapter("HQ", CustomAdapter09::class.java) //Empty
            registerPoiTypeAdapter("Metropolia Motorsport", CustomAdapter09::class.java) //Empty

            registerPoiTypeAdapter("mm", CustomAdapter01::class.java) //metropolia motorsport
            registerPoiTypeAdapter("hsl", CustomAdapter02::class.java) //APItext
            registerPoiTypeAdapter("Urban Farm Lab", CustomAdapter03::class.java) //urbanfl
            registerPoiTypeAdapter("Electria", CustomAdapter04::class.java) //electria
            registerPoiTypeAdapter("toilets", CustomAdapter05::class.java) //toilet 3D
            registerPoiTypeAdapter("Sodexo", CustomAdapter06::class.java) //APItext
            registerPoiTypeAdapter("Garage", CustomAdapter07::class.java) //garage
            registerPoiTypeAdapter("akvaario", CustomAdapter08::class.java) //Text
            registerPoiTypeAdapter("cafe", CustomAdapter10::class.java) //cafe
            registerPoiTypeAdapter("info", CustomAdapter11::class.java) //APItext
            registerPoiTypeAdapter("metropolia", CustomAdapter12::class.java) //metropolia logo
            registerPoiTypeAdapter("food", CustomAdapter13::class.java) //Sodexo logo
            registerPoiTypeAdapter("tiedote", CustomAdapter14::class.java) //APItext
            registerPoiTypeAdapter("hsllogo", CustomAdapter15::class.java) //hsl logo

        //PoiTypes.POI / DESTINATION //create POI type as any string=""=PoiTypes.POI
        //TextPoiTypeAdapter::class.java
        //anywhere else: class customPoiAdapter: TextPoiTypeAdapter
        //override fun createRenderable{}...
        requireActivity().onBackPressedDispatcher.addCallback(this, object: OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!onBackPressed()) {
                    findNavController().popBackStack()
                }
            }
        })
        return binding.root
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        //do not call super
    }

    private var searchMenuItem: MenuItem? = null

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.navigation_fragment, menu)    //todo
        searchMenuItem = menu.findItem(R.id.menu_action_search)
    }

    /**
     * We can override here the back button behavior
     * We want to stop navigation first, then hide the minimap view
     * returning false will result in default action -> removing fragment
     */
    fun onBackPressed(): Boolean {
        if (service.isNavigating) {
            service.stopNavigation()
            return true
        } else {
            val showing = (binding.isMinimapShowing as Boolean?) ?: false
            if (showing) {
                showMinimap(false)
                return true
            }
        }
        return false
    }

    private fun showMinimap(show: Boolean) {
        binding.isMinimapShowing = show
        navigationArrowNode.gravity =
            if (show) NavigationArrowNode.Gravity.CENTER else NavigationArrowNode.Gravity.BOTTOM
    }

    override fun onServiceAvailable(service: VimaiService) {

        // customization for search bar
        searchMenuItem?.let { searchMenuItem ->
            PoiSearchWrapper(service.buildingID, connection)
                .configure(searchMenuItem, { pois, _ ->
                    service.poiList = pois
                    //make the type of each POI identical to its name
                    pois.forEach { poi ->
                        poi.type = poi.name
                    }

                }, { context, cursor ->
                    SimpleCursorAdapter(context, R.layout.item_search_suggestion, cursor,
                        arrayOf(PoiSearchWrapper.COLUMN_NAME, PoiSearchWrapper.COLUMN_DESCRIPTION),
                        intArrayOf(R.id.tv_name, R.id.tv_description), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
                })
        }

        // navigation handling
        service.navigationObservable.observeOn(AndroidSchedulers.mainThread()).subscribe { event ->
            when(event) {
                is NavigationStarted -> showNavigationInfo(event.navigator.destinationPoi!!, event.navigator.navigationProgressObservable)
                is NavigationReRouted -> showNavigationInfo(event.navigator.destinationPoi!!, event.navigator.navigationProgressObservable)
                is NavigationEnded -> {
                    hideNavigationInfo()
                    if (event.reached) {
                        Toast.makeText(context, R.string.you_have_arrived, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }.also { disposables.add(it) }

        // poi press handler
        service.poiSelectedObservable.subscribe { poi ->
            navigateTo(poi)
        }.also { disposables.add(it) }

        initializeMinimap()
    }

    private fun initializeMinimap() {
        var minimapFragment = childFragmentManager.findFragmentByTag(MINIMAP_FRAGMENT_TAG) as MinimapFragment?
        if (minimapFragment == null || !minimapFragment.isAdded) {
            minimapFragment = MinimapFragment.newInstance(service.currentAreas)
            childFragmentManager.beginTransaction().replace(R.id.minimapLayout, minimapFragment).commit()
        }

        //minimap userview and destination customization
        minimapFragment.userView = UserLocationView(
            BitmapFactory.decodeResource(resources, R.drawable.user_location),
            BitmapFactory.decodeResource(resources, R.drawable.user_location_approximate))
        minimapFragment.destinationView = DestinationView(context!!, getBitmapFromVectorDrawable(context!!, fi.vimai.navigation.R.drawable.vimai_minimap_target))

        //when a user presses the minimap, navigate to that location
        minimapFragment.minimapLongPressObservable.subscribe { position ->
            Log.d("Position", "$position")
            destinationPoi = Poi.createDestination(position).also { navigateTo(it) }

        }.also { disposables.add(it) }
    }

    override fun onServiceUnavailable() {
        disposables.dispose()
    }

    override fun onUserTrackingStarted() {
        super.onUserTrackingStarted()
        showMinimap(true)
    }

    override fun onInitialized(arSceneView: ArSceneView) {
        navigationPathMaterial = MaterialFactory.makeTransparentWithColor(context!!,
            Color(246 / 255f, 167 / 255f, 35 / 255f))

        resources3D = loadResources()
        initScene()
    }

    private fun loadResources(): CompletableFuture<Void> {
        val pinStage = ModelRenderable.builder().setSource(context, Uri.parse("src\\main\\assets\\cube.sfb")).build()

        return CompletableFuture.allOf(pinStage).thenApply {
            pinRenderable = pinStage.get()
            return@thenApply null
        }
    }

    private fun initScene() {
        resources3D?.handle { _, throwable ->
            if (throwable != null) {
                throwable.printStackTrace()
                return@handle
            }

            //todo check this
//            navigationArrowRenderable = andyRenderable  //just like that we can change the navigation arrow renderable

//            val pinNode = Node()
//            pinNode.renderable = andyRenderable
//            pinNode.localPosition = Vector3(1f, 0f, -1f)
//            pinNode.worldScale = Vector3(0.5f, 0.5f, 0.5f)
//            pinNode.setParent(worldParent)
        }
    }

    private fun navigateTo(poi: Poi) {
        service.planRouteTo(poi).flatMap { route ->
            service.startNavigation()
        }.observeOn(AndroidSchedulers.mainThread()).subscribe({}, {t ->
            when (t) {
                is CannotReachDestinationException -> showUserError(context, t)
                is UserPositionNotAvailableException -> showUserError(context, t)
                is DestinationTooCloseException -> showUserError(context, t)
                else -> view?.let { showGenericError(it) }
            }
        }).also { connection.disposables.add(it) }
        Log.d("DBG", "Poi lista:${poi.name}")
    }

    private var navigationInfoViewDisposable: Disposable? = null

    private fun showNavigationInfo(destinationPoi: Poi, progressObservable: Observable<NavigationProgress>) {
        binding.isNavigating = true
        navigationInfoViewDisposable?.dispose()
        navigationInfoViewDisposable = progressObservable.observeOn(AndroidSchedulers.mainThread())
            .subscribe { progress ->
                binding.tvNavigationInfo.text = getString(R.string.navigation_info, destinationPoi.name, progress.metersRemaining, progress.secondsRemaining)
            }.also { disposables.add(it) }
    }

    private fun hideNavigationInfo() {
        binding.isNavigating = false
        navigationInfoViewDisposable?.dispose()
    }
}
