package fi.vimai.client.example.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fi.vimai.client.example.R
import fi.vimai.navigation.server.entities.Building
import fi.vimai.navigation.service.VimaiService
import fi.vimai.navigation.service.VimaiServiceConnection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

/**
 * Fragment showing the premises list
 */
class BuildingsFragment : Fragment() {


    private var mRecyclerView: RecyclerView? = null
    private var rootLayout: ViewGroup? = null

    private var progressBar: ProgressBar? = null

    private val itemClickListener = object :
        BuildingsAdapter.OnItemClickListener {
        override fun onItemClick(item: Building) {
            val args = bundleOf("building" to item, "title" to item.name)
            findNavController().navigate(R.id.action_buildingsFragment_to_overviewFragment, args)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootLayout = inflater.inflate(R.layout.fragment_buildings, container, false) as ViewGroup

        mRecyclerView = rootLayout!!.findViewById(R.id.rv_premises)
        mRecyclerView!!.layoutManager = LinearLayoutManager(context)

        progressBar = rootLayout!!.findViewById(R.id.progressBar)
        progressBar?.visibility = View.VISIBLE
        connection.bind(context!!)
        return rootLayout
    }

    private val connection = object : VimaiServiceConnection() {
        override fun onServiceAvailable(service: VimaiService) {
            loadData(service)
        }
    }

    private fun loadData(service: VimaiService) {
        service.api.getBuildingsRx().observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { buildings ->
                    val adapter =
                        BuildingsAdapter(
                            context!!,
                            buildings
                        )
                    adapter.onItemClickListener = itemClickListener
                    mRecyclerView!!.adapter = adapter
                    progressBar?.visibility = View.GONE
                },
                {t ->
                    t.printStackTrace()
                    progressBar?.visibility = View.GONE
                }).also { connection.disposables.add(it) }
    }

    override fun onDestroyView() {
        connection.unbind()
        super.onDestroyView()
    }

    private class BuildingsAdapter internal constructor(
        private val mContext: Context,
        private val feedItemList: List<Building>?
    ) : RecyclerView.Adapter<BuildingsAdapter.CustomViewHolder>() {

        var onItemClickListener: OnItemClickListener? = null
            internal set

        internal interface OnItemClickListener {
            fun onItemClick(item: Building)
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CustomViewHolder {
            val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_buildings_list, viewGroup, false)
            return CustomViewHolder(view)
        }

        override fun onBindViewHolder(customViewHolder: CustomViewHolder, i: Int) {
            val building = feedItemList!![i]
            if (onItemClickListener != null) {
                customViewHolder.parent.setOnClickListener { onItemClickListener!!.onItemClick(building) }
            }
            //Setting text view title
            customViewHolder.title.text = building.name
            customViewHolder.subtitle.text = building.description
        }

        override fun getItemCount(): Int {
            return feedItemList?.size ?: 0
        }

        internal inner class CustomViewHolder(var parent: View) : RecyclerView.ViewHolder(parent) {
            protected var image: ImageView
            var title: TextView
            var subtitle: TextView

            init {
                this.title = parent.findViewById(R.id.title)
                this.subtitle = parent.findViewById(R.id.subtitle)
                this.image = parent.findViewById(R.id.image)
            }
        }
    }

}
